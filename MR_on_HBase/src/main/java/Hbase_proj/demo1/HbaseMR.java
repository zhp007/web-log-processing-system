package Hbase_proj.demo1;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class HbaseMR {
	
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException{
		
		Configuration conf = HBaseConfiguration.create();

		Job job = new Job(conf, "HbaseMR");
		job.setJarByClass(HbaseMR.class);
		
		Scan scan = new Scan();
		scan.setCaching(2000);
		scan.setCacheBlocks(false);
		
		TableMapReduceUtil.initTableMapperJob(
			"IP2LocationDB", scan, Mapper.class, Text.class, IntWritable.class, job
		);
		job.setReducerClass(MyReducer.class);
		job.setNumReduceTasks(1);
		
		String output = "hdfs://quickstart.cloudera:8020/user/cloudera/output/HbaseMR/test";
		FileOutputFormat.setOutputPath(job, new Path(output));
		job.waitForCompletion(true);
	
	}	
}

class Mapper extends TableMapper<Text, IntWritable>{

	private final IntWritable one = new IntWritable(1);
	
	@Override
	protected void map(ImmutableBytesWritable key,Result value, Context context)
			throws IOException, InterruptedException {
		
		Text k = new Text(Bytes.toString(value.getValue(Bytes.toBytes("location"), Bytes.toBytes("country"))));
		context.write(k, one);
		
	}
	
}

// TableReducer: write to Hbase
// like "Put put = ... context.write(null, put)"

class MyReducer extends Reducer<Text, IntWritable, Text, IntWritable>{

	private IntWritable result = new IntWritable();
	
	@Override
	protected void reduce(Text key, Iterable<IntWritable> values, Context context)
			throws IOException, InterruptedException {
		int sum = 0;
		for(IntWritable value: values){
			sum += value.get();
		}
		result.set(sum);
		
		context.write(key, result);		
	}	
}


