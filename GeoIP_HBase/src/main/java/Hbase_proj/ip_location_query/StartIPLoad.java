package Hbase_proj.ip_location_query;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class StartIPLoad {
	
	public void readFilesAndLoad(String startIpPath, long[] sIPs){
		File localInputFolder = new File(startIpPath);
		File[] listOfFiles = localInputFolder.listFiles();
		int count = 0;
		for(File file: listOfFiles){
			BufferedReader br = null;
			if(file.getName().endsWith("csv")){
				try{
					
					String sCurrentLine;
					br = new BufferedReader(new FileReader(file));
					
					while((sCurrentLine = br.readLine()) != null){
						String[] strIPs = sCurrentLine.split(",");
						for(String strIP: strIPs){
							sIPs[count++] = Long.parseLong(strIP);
						}
					}
					
				}catch(IOException e){
					e.printStackTrace();
				}finally{
					try{
						if(br != null){
							br.close();
						}
					}catch(IOException ex){
						ex.printStackTrace();
					}
				}
			}
		}
	}
	
	public static void main(String[] args){
		
		String startIpPath = "/home/cloudera/hbase_data/StartIP";
		long[] sIPs = new long[3815620];
		StartIPLoad startIPLoad = new StartIPLoad();
		startIPLoad.readFilesAndLoad(startIpPath, sIPs);
		System.out.println(sIPs[1234567]);

	}

}
