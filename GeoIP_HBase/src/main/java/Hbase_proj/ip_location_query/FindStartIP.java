package Hbase_proj.ip_location_query;

public class FindStartIP {
	
	long[] startIps;
	
	public FindStartIP(long[] sIps){
		startIps = new long[sIps.length];
		for(int i = 0; i < sIps.length; i++){
			startIps[i] = sIps[i];
		}
	}
	
	public long search(long targetIp){
		int left = 0, right = startIps.length;
		while(left + 1 < right){
			int mid = left + (right - left) / 2;
			if(startIps[mid] > targetIp){
				right = mid - 1;
			}else{
				left = mid;
			}
		}
		return startIps[left];
	}
	
	public static void main(String[] args){
		long[] startIps = {181614331, 181614337, 181614348};
		FindStartIP obj = new FindStartIP(startIps);
		long result = obj.search(181614338);
		System.out.println(result);
	}
}
