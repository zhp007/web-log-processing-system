package Hbase_proj.ip_location_query;

public class IPv4ToLong {
	
	public long ipToLong(String ipAddress){
		
		String[] ipAddressInArray = ipAddress.split("\\.");
		long result = 0;
		
		for(int i = 0; i < ipAddressInArray.length; i++){
			int power = 3 - i;
			int ip = Integer.parseInt(ipAddressInArray[i]);
			result += ip * Math.pow(256, power);
		}
		
		return result;
		
	}
	
	public static void main(String[] args){
		IPv4ToLong obj = new IPv4ToLong();
		long result = obj.ipToLong("10.211.55.2");
		System.out.println(result);
	}

}
