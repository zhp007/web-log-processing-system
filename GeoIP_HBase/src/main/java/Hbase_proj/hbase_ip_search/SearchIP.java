package Hbase_proj.hbase_ip_search;

import org.apache.hadoop.conf.*;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import Hbase_proj.ip_location_query.*;

public class SearchIP {
	static Configuration conf = HBaseConfiguration.create();
	
	public static void main(String[] args) throws Exception{
		
		Connection connection = ConnectionFactory.createConnection(conf);
		Table table = connection.getTable(TableName.valueOf("IP2LocationDB"));
		
		IPv4ToLong ipv4ToLong = new IPv4ToLong();		
		long longIP = ipv4ToLong.ipToLong("31.184.238.202");
		
		long[] sIPs = new long[3815620];
		String startIpPath = "/home/cloudera/hbase_data/StartIP";
		StartIPLoad startIPLoad = new StartIPLoad();
		startIPLoad.readFilesAndLoad(startIpPath, sIPs);
		
		FindStartIP findStartIP = new FindStartIP(sIPs);
		long startIP = findStartIP.search(longIP);
		String strIP = String.valueOf(startIP);
		
		Get get = new Get(strIP.getBytes());
		Result getResult = table.get(get);
		String country = Bytes.toString(getResult.getValue("location".getBytes(), "country".getBytes()));
		String region = Bytes.toString(getResult.getValue("location".getBytes(), "region".getBytes()));
		
		System.out.println("31.184.238.202" + ":" + country + "," + region);
		
	}
	
}
