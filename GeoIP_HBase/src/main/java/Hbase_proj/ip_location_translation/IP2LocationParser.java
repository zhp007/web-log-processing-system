package Hbase_proj.ip_location_translation;

public class IP2LocationParser {
	String startIp;
	String country;
	String region;
	
	public void parse(String record){
		String[] vals = record.split(",");
		startIp = vals[0];
		country = vals[1];
		region = vals[2];
	}

	public String getStartIp() {
		return startIp;
	}

	public void setStartIp(String startIp) {
		this.startIp = startIp;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

}
