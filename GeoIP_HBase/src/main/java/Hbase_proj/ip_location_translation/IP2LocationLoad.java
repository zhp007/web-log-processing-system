package Hbase_proj.ip_location_translation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.RetriesExhaustedWithDetailsException;
import org.apache.hadoop.hbase.util.Bytes;
//import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class IP2LocationLoad {
	
	static Configuration conf = HBaseConfiguration.create();
	static IP2LocationParser iP2LocationParser= new IP2LocationParser();
	static Table table;
	
	static final Logger logger = Logger.getLogger(IP2LocationLoad.class);
	
	public static Put buildPutList(Table table, IP2LocationParser IP2LocationRecord)
			throws RetriesExhaustedWithDetailsException, InterruptedIOException, IOException {
		
		Put put = new Put(Bytes.toBytes(IP2LocationRecord.getStartIp()));
		
		put.addColumn(Bytes.toBytes("location"), Bytes.toBytes("country"), 
				Bytes.toBytes(IP2LocationRecord.getCountry()));
		put.addColumn(Bytes.toBytes("location"), Bytes.toBytes("region"), 
				Bytes.toBytes(IP2LocationRecord.getRegion()));
		
		return put;
		
	}
	
	public static void loadPutList(List<Put> puts, Table table) throws IOException {
		table.put(puts);
	}
	
	public static void readFilesAndLoad(Table table, String iP2LocationPath) {
		int counter = 1;
		
		List<Put> puts = new ArrayList<Put>();
		
		File localInputFolder = new File(iP2LocationPath);
		File[] listOfFiles = localInputFolder.listFiles();
		for (File file : listOfFiles) {
			BufferedReader br = null;
			if (file.getName().endsWith("csv")) {
				try {
					
					String sCurrentLine;
					br = new BufferedReader(new FileReader(file));
					
					while((sCurrentLine = br.readLine()) != null){
						if(++counter%10000 == 0){
							logger.info(counter);
						}
						iP2LocationParser.parse(sCurrentLine);
						puts.add(buildPutList(table, iP2LocationParser));
						if(counter >= 200000){
							loadPutList(puts, table);
							puts.clear();
							counter = 1;
						}
					}
					if(puts.size() > 0){
						loadPutList(puts, table);
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					try {
						if(br != null){
							br.close();
						}
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
		}
		
	}
	
	public static void main(String[] args) throws IOException {
		
		Connection connection = ConnectionFactory.createConnection(conf);
		Table table = connection.getTable(TableName.valueOf("IP2LocationDB"));
		
		String raw_data = "/home/cloudera/hbase_data/IP2Location";
		
		readFilesAndLoad(table, raw_data);
		
		table.close();
		connection.close();
		
	}

}
