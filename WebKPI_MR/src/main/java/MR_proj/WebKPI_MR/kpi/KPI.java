package MR_proj.WebKPI_MR.kpi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
//import java.util.HashSet;
import java.util.Locale;
//import java.util.Set;

/*
 * KPI Object
 */
public class KPI {
	private String remote_addr;// ip address
	private String remote_user;// user name, default = '-'
	private String time_local;// time(zone)
	private String request;// http protocol
	private String status;// status, 200 if success
	private String body_bytes_sent;// size of file sent to client
	private String http_referer;// from which link comes this page
	private String http_user_agent;// record client web browser
	private boolean valid = true;// this record is valid or not

	private static KPI parser(String line) {
		//System.out.println(line);
		KPI kpi = new KPI();
		String[] arr = line.split(" ");
		if (arr.length > 11) {
			kpi.setRemote_addr(arr[0]);
			kpi.setRemote_user(arr[1]);
			kpi.setTime_local(arr[3].substring(1));
			kpi.setRequest(arr[6]);
			kpi.setStatus(arr[8]);
			kpi.setBody_bytes_sent(arr[9]);
			kpi.setHttp_referer(arr[10]);
			if (arr.length > 12) {
				kpi.setHttp_user_agent(arr[11] + " " + arr[12]);
			} else {
				kpi.setHttp_user_agent(arr[11]);
			}
			if (Integer.parseInt(kpi.getStatus()) >= 400) {// if status code > 400, error!
				kpi.setValid(false);
			}
		} else {
			kpi.setValid(false);
		}
		return kpi;
	}

	/**
	 * PV
	 */
	public static KPI filterPVs(String line) {
		/*KPI kpi = parser(line);
		Set<String> pages = new HashSet<String>();
		pages.add("/logs/access.log");
		pages.add("/robots.txt");
		if (!pages.contains(kpi.getRequest())) {
			kpi.setValid(false);
		}
		return kpi;*/
		return parser(line);
	}

	/**
	 * Unique IP
	 */
	public static KPI filterIPs(String line) {
		/*KPI kpi = parser(line);
		Set<String> ips = new HashSet<String>();
		ips.add("180.76.15.19");
		ips.add("31.184.238.174");
		ips.add("40.77.167.52");
		if (!ips.contains(kpi.getRemote_addr())) {
			kpi.setValid(false);
		}
		return kpi;*/
		return parser(line);
	}

	/**
	 * Web Browser
	 */
	public static KPI filterBroswer(String line) {
		return parser(line);
	}

	/**
	 * Hour
	 */
	public static KPI filterTime(String line) {
		return parser(line);
	}

	/**
	 * Domain
	 */
	public static KPI filterDomain(String line) {
		return parser(line);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("valid:" + this.valid);
		sb.append("\nremote_addr:" + this.remote_addr);
		sb.append("\nremote_user:" + this.remote_user);
		sb.append("\ntime_local:" + this.time_local);
		sb.append("\nrequest:" + this.request);
		sb.append("\nstatus:" + this.status);
		sb.append("\nbody_bytes_sent:" + this.body_bytes_sent);
		sb.append("\nhttp_referer:" + this.http_referer);
		sb.append("\nhttp_user_agent:" + this.http_user_agent);
		return sb.toString();
	}

	public String getRemote_addr() {
		return remote_addr;
	}

	public void setRemote_addr(String remote_addr) {
		this.remote_addr = remote_addr;
	}

	public String getRemote_user() {
		return remote_user;
	}

	public void setRemote_user(String remote_user) {
		this.remote_user = remote_user;
	}

	public String getTime_local() {
		return time_local;
	}

	public Date getTime_local_Date() throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss",
				Locale.US);
		return df.parse(this.time_local);
	}

	public String getTime_local_Date_hour() throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHH");
		return df.format(this.getTime_local_Date());
	}

	public void setTime_local(String time_local) {
		this.time_local = time_local;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBody_bytes_sent() {
		return body_bytes_sent;
	}

	public void setBody_bytes_sent(String body_bytes_sent) {
		this.body_bytes_sent = body_bytes_sent;
	}

	public String getHttp_referer() {
		return http_referer;
	}

	public String getHttp_referer_domain() {
		if (http_referer.length() < 8) {
			return http_referer;
		}
		String str = this.http_referer.replace("\"", "").replace("http://", "")
				.replace("https://", "");
		return str.indexOf("/") > 0 ? str.substring(0, str.indexOf("/")) : str;
	}

	public void setHttp_referer(String http_referer) {
		this.http_referer = http_referer;
	}

	public String getHttp_user_agent() {
		return http_user_agent;
	}

	public void setHttp_user_agent(String http_user_agent) {
		this.http_user_agent = http_user_agent;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public static void main(String args[]) {
		String line = "180.76.15.19 - - [31/May/2016:17:05:48 -0700] \"GET /accessories/shelf_buddy HTTP/1.1\" 200 5653 \"-\" \"Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)\" \"www.chimscan.net\"";
		System.out.println(line);
		KPI kpi = new KPI();
		String[] arr = line.split(" ");
		kpi.setRemote_addr(arr[0]);
		kpi.setRemote_user(arr[1]);
		kpi.setTime_local(arr[3].substring(1));
		kpi.setRequest(arr[6]);
		kpi.setStatus(arr[8]);
		kpi.setBody_bytes_sent(arr[9]);
		kpi.setHttp_referer(arr[10]);
		kpi.setHttp_user_agent(arr[11] + " " + arr[12]);
		System.out.println(kpi);
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd:HH:mm:ss z",Locale.US);
			System.out.println(df.format(kpi.getTime_local_Date()));
			System.out.println(kpi.getTime_local_Date_hour());
			System.out.println(kpi.getHttp_referer_domain());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
