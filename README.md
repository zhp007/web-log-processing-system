# Apache Access Log Data Processing System#

Following is the code structure and description.

### /GeoIP_HBase ###

* Store preprocessed IP2Location LITE geolocation database V3 into HBase
* Query IP geoLocation directly on Hbase
* Cache database in Memcached to support real-time query

### /MR_on_HBase ###

* Run MapReduce jobs on HBase to collect geolocation of web visitors

### /WebKPI_MR ###

* Create MapReduce jobs to collect basic KPIs from the access log, such as PV, IP, Referrer, Agent and etc.


### /IP_Location_Python ###

* Some Python scripts to preprocess geolocation csv file.

### /SparkStreaming_Log ###
* Use Netcat to simulate the access log stream.
* Build Spark Streaming code with Scala to track real-time top URLs, popular referrer page and etc.
* Create Log error alarm


