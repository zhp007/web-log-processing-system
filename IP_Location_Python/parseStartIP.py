input_csv = 'IP2LOCATION-LITE-DB3-v2.csv'
output_csv = 'startIP-DB.csv'
#input_csv = 'ip_location_test.csv'
#output_csv = 'startIps_test.csv'
f1 = open(input_csv, 'r')
reader = csv.reader(f1, delimiter=',')
with open(output_csv, 'wb') as f2:
    writer = csv.writer(f2, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    tmp = []
    count = 0
    for row in reader:
        if count == 10:
            writer.writerow(tmp)
            tmp = []
            count = 0
        tmp += [row[0]]
        count += 1
    if len(tmp) > 0:
        writer.writerow(tmp)