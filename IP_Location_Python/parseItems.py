import csv
input_csv = 'IP2LOCATION-LITE-DB3.CSV'
output_csv = 'IP2LOCATION-LITE-DB3-v2.CSV'
f1 = open(input_csv, 'r')
reader = csv.reader(f1, delimiter=',')
with open(output_csv, 'wb') as f2:
    writer = csv.writer(f2, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    for row in reader:
        writer.writerow([row[0], row[3], row[4]])